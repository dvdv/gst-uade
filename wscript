#!/usr/bin/env python3

top = '.'
out = 'build'


def options(opt):
	opt.add_option('--enable-debug', action = 'store_true', default = False, help = 'enable debug build [default: %default]')
	opt.add_option('--with-package-name', action = 'store', default = "gstnonstreamaudio plug-in source release", help = 'specify package name to use in plugin [default: %default]')
	opt.add_option('--with-package-origin', action = 'store', default = "Unknown package origin", help = 'specify package origin URL to use in plugin [default: %default]')
	opt.add_option('--plugin-install-path', action = 'store', default = "${PREFIX}/lib/gstreamer-1.0", help = 'where to install the plugin for GStreamer 1.0 [default: %default]')
	opt.load('compiler_c')


def configure(conf):
	import os

	conf.load('compiler_c')

	compiler_flags = ['-Wextra', '-Wall', '-std=gnu99', '-pedantic', '-fPIC', '-DPIC']

	if conf.options.enable_debug:
		compiler_flags += ['-O0', '-g3', '-ggdb']
	else:
		compiler_flags += ['-O2', '-s']

	conf.env['CFLAGS'] += compiler_flags

	conf.check_cfg(package = 'gstreamer-1.0 >= 1.14.0',           uselib_store = 'GSTREAMER', args = '--cflags --libs', mandatory = 1)
	conf.check_cfg(package = 'gstreamer-base-1.0 >= 1.14.0',      uselib_store = 'GSTREAMER', args = '--cflags --libs', mandatory = 1)
	conf.check_cfg(package = 'gstreamer-audio-1.0 >= 1.14.0',     uselib_store = 'GSTREAMER', args = '--cflags --libs', mandatory = 1)
	conf.check_cfg(package = 'gstreamer-bad-audio-1.0 >= 1.14.0', uselib_store = 'GSTREAMER', args = '--cflags --libs', mandatory = 1)
	conf.env['PLUGIN_INSTALL_PATH'] = os.path.expanduser(conf.options.plugin_install_path)

	conf.check_cfg(package = 'libuade', uselib_store = 'UADE', args = '--cflags --libs', mandatory = 1)
	conf.check_cc(lib = 'bencodetools', uselib_store = 'UADE', mandatory = 1)

	conf.define('GST_PACKAGE_NAME', conf.options.with_package_name)
	conf.define('GST_PACKAGE_ORIGIN', conf.options.with_package_origin)
	conf.define('PACKAGE', "gstuade")
	conf.define('VERSION', "1.0")
	conf.write_config_header('config.h')


def build(bld):
	bld(
		features = ['c', 'cshlib'],
		includes = ['.'],
		uselib = 'GSTREAMER UADE',
		target = 'gstuade',
		source = bld.path.ant_glob('ext/uade/*.c'),
		defines = ['HAVE_CONFIG_H'],
		install_path = bld.env['PLUGIN_INSTALL_PATH']
	)
