gst-uade
========

About
-----

This is a [GStreamer 1.0](http://gstreamer.freedesktop.org/) plugin for decoding Amiga music files from demos and video games using the [Unix Amiga Delitracker Emulator (UADE)](http://zakalwe.fi/uade). [Here is its Gitlab repository.](https://gitlab.com/uade-music-player/uade)

In this plugin, we distinguish between two types of Amiga music: "raw" and "RMC".

"Raw" means that the music files are typically game rips. Some of this raw music data may consist of multiple files; for example, a song in the TFMX format consists of an MDAT and a SMPL file. The title music of Apidya for example is made up of the files `mdat.title` and `smpl.title`. For this reason, with raw files, UADE needs the filename directly - it cannot just rely on data coming in from a sinkpad, because it has to access the accompanying files (in the Apidya example, `smpl.title` is an accompanying file) as well.

[RMC](https://gitlab.com/uade-music-player/rmc) is an initiative to provide a container format for these raw files. With RMC, there is always only one file. In addition, RMC files can contain metadata that might otherwise not be present (like the song length). When decoding RMC files with UADE, the data *can* come from a sinkpad, because there never are any accompanying files.


License
-------

These plugins are licensed under the LGPL v2.1.


Available GStreamer elements
----------------------------

* `uaderawdec` : UADE based decoder element for raw Amiga music.
* `uadermcdec` : UADE based decoder element for UADE RMC files.

`uaderawdec` is classified as a source element, has no sinkpad, and implements a URI handler for `uade://` URIs. The rest of the URI is a filename with absolute path. For example, if the Apidya title song is at `/home/test/Music/Apidya/mdat.title`, then it is possible to play it with the `gst-play` command line tool by using the URI `uade:///home/test/Music/Apidya/mdat.title`.

`uadermcdec` is *not* classified as a source element, and does have a sinkpad.


Dependencies
------------

You'll need a GStreamer installation (version 1.14.0 or newer), and UADE with libification support. (At time of writing this - 2018-09-16 - the current version of UADE in Gitlab has this feature.


Building and installing
-----------------------

This project uses the [waf meta build system](https://code.google.com/p/waf/). To configure , first set
the following environment variables to whatever is necessary for cross compilation for your platform:

* `CC`
* `CFLAGS`
* `LDFLAGS`
* `PKG_CONFIG_PATH`
* `PKG_CONFIG_SYSROOT_DIR`

Then, run:

    ./waf configure --prefix=PREFIX

(The aforementioned environment variables are only necessary for this configure call.)
PREFIX defines the installation prefix, that is, where the built binaries will be installed.

Once configuration is complete, run:

    ./waf

This builds the plugins.
Finally, to install, run:

    ./waf install


Additional configuration switches are:

* `--enable-debug` : Enables a build with optimizations turned off and debug symbols turned on
* `--with-package-name` : Specifies the package name for the GStreamer plugin ([see here for details](https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gstreamer/html/GstPlugin.html#GstPluginDesc))
* `--with-package-origin` : Specifies the package origin for the GStreamer plugin ([see here for details](https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gstreamer/html/GstPlugin.html#GstPluginDesc))
* `--plugin-install-path` : Where to install the plugin; by default, it is installed in `$PREFIX/lib`, but on some installations, it might be useful to directly specify the installation path


Example gst-launch pipelines
----------------------------

`gst-launch-1.0 playbin uri=uade:///home/test/Music/Apidya/mdat.title`

`gst-launch-1.0 playbin uri=file:///home/test/Music/Apidya/title.rmc`

`gst-launch-1.0 uaderawdec location=/home/test/Music/Apidya/mdat.title ! audioconvert ! audioresample ! autoaudiosink`

`gst-launch-1.0 filesrc location=/home/test/Music/Apidya/title.rmc ! uadermcdec ! audioconvert ! audioresample ! autoaudiosink`
