/*
 *   GStreamer plugin for decoding Amiga music with the Unix Amiga Delitracker Emulator (UADE)
 *   Copyright (C) 2018 Carlos Rafael Giani
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


#ifdef HAVE_CONFIG_H
 #include <config.h>
#endif

#include "gstuade.h"
#include "gstuaderawdec.h"
#include "gstuadermcdec.h"

#include <uade/uade.h>


GType gst_uade_filter_type_get_type(void)
{
	static GType gst_uade_filter_type_type = 0;

	if (!gst_uade_filter_type_type)
	{
		static GEnumValue filter_type_values[] =
		{
			{ GST_UADE_FILTER_TYPE_A500,  "Amiga 500 lowpass filter", "a500" },
			{ GST_UADE_FILTER_TYPE_A1200, "Amiga 1200 lowpass filter", "a1200" },
			{ 0, NULL, NULL },
		};

		gst_uade_filter_type_type = g_enum_register_static(
			"UadeFilterType",
			filter_type_values
		);
	}

	return gst_uade_filter_type_type;
}


GType gst_uade_headphone_mode_get_type(void)
{
	static GType gst_uade_headphone_mode_type = 0;

	if (!gst_uade_headphone_mode_type)
	{
		static GEnumValue headphone_mode_values[] =
		{
			{ GST_UADE_HEADPHONE_MODE_NONE, "No headphone mode", "none" },
			{ GST_UADE_HEADPHONE_MODE_1,    "Headphone mode 1", "mode1" },
			{ GST_UADE_HEADPHONE_MODE_2,    "Headphone mode 2", "mode2" },
			{ 0, NULL, NULL },
		};

		gst_uade_headphone_mode_type = g_enum_register_static(
			"UadeHeadphoneMode",
			headphone_mode_values
		);
	}

	return gst_uade_headphone_mode_type;
}


GType gst_uade_vertical_timing_mode_get_type(void)
{
	static GType gst_uade_vertical_timing_mode_type = 0;

	if (!gst_uade_vertical_timing_mode_type)
	{
		static GEnumValue vertical_timing_mode_values[] =
		{
			{ GST_UADE_VERTICAL_TIMING_MODE_PAL,  "PAL timing", "pal" },
			{ GST_UADE_VERTICAL_TIMING_MODE_NTSC, "NTSC timing", "ntsc" },
			{ 0, NULL, NULL },
		};

		gst_uade_vertical_timing_mode_type = g_enum_register_static(
			"UadeVerticalTimingMode",
			vertical_timing_mode_values
		);
	}

	return gst_uade_vertical_timing_mode_type;
}


// The prefix is made of the RMC magic bytes and a 3-byte bencode prefix
#define RMC_PREFIX_LEN (RMC_MAGIC_LEN + 3)

static void gst_rmc_parse_type_find(GstTypeFind *tf, G_GNUC_UNUSED gpointer user_data)
{
	guint8 const *data;

	if ((data = gst_type_find_peek(tf, 0, RMC_PREFIX_LEN)) != NULL)
	{
		if (uade_is_rmc((char const *)data, RMC_PREFIX_LEN))
		{
#if GST_CHECK_VERSION(1, 20, 0)
			gst_type_find_suggest_empty_simple(tf, GST_TYPE_FIND_LIKELY, GST_UADE_RMC_MEDIA_TYPE);
#else
			GstCaps *caps = gst_caps_new_empty_simple(GST_UADE_RMC_MEDIA_TYPE);
			gst_type_find_suggest(tf, GST_TYPE_FIND_LIKELY, caps);
			gst_caps_unref(caps);
#endif
		}
	}
}




static gboolean plugin_init(GstPlugin *plugin)
{
	if (!gst_element_register(plugin, "uaderawdec", GST_RANK_SECONDARY, gst_uade_raw_dec_get_type()))
		return FALSE;
	if (!gst_element_register(plugin, "uadermcdec", GST_RANK_SECONDARY, gst_uade_rmc_dec_get_type()))
		return FALSE;
	if (!gst_type_find_register(plugin, GST_UADE_RMC_MEDIA_TYPE, GST_RANK_PRIMARY, gst_rmc_parse_type_find, "rmc", NULL, NULL, NULL))
		return FALSE;
	return TRUE;
}

GST_PLUGIN_DEFINE(
	GST_VERSION_MAJOR,
	GST_VERSION_MINOR,
	uade,
	"UADE Amiga music player",
	plugin_init,
	"1.0",
	"LGPL",
	"package",
	"http://no-url-yet"
)


