/*
 *   GStreamer plugin for decoding Amiga music with the Unix Amiga Delitracker Emulator (UADE)
 *   Copyright (C) 2018 Carlos Rafael Giani
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


#ifdef HAVE_CONFIG_H
 #include <config.h>
#endif

#include "gstuaderawdec.h"
#include "gstuadebasedec.h"


GST_DEBUG_CATEGORY_STATIC(uaderawdec_debug);
#define GST_CAT_DEFAULT uaderawdec_debug


enum
{
	PROP_0,
	PROP_LOCATION
};


#define DEFAULT_LOCATION NULL


static gchar const *uade_supported_protocols[] = { "uade", NULL };
static gchar const uade_uri_scheme[] = "uade://";




struct _GstUadeRawDec
{
	GstUadeBaseDec parent;

	gchar *location;
};


struct _GstUadeRawDecClass
{
	GstUadeBaseDecClass parent_class;
};



static void gst_uade_raw_dec_uri_handler_init(gpointer iface, gpointer iface_data);
static GstURIType gst_uade_raw_dec_uri_get_type(GType type);
static const gchar* const * gst_uade_raw_dec_uri_get_protocols(GType type);
static gchar* gst_uade_raw_dec_uri_get_uri(GstURIHandler *handler);
static gboolean gst_uade_raw_dec_uri_set_uri(GstURIHandler *handler, const gchar *uri, GError **error);


G_DEFINE_TYPE_WITH_CODE(
	GstUadeRawDec, gst_uade_raw_dec, GST_TYPE_UADE_BASE_DEC,
	G_IMPLEMENT_INTERFACE(GST_TYPE_URI_HANDLER, gst_uade_raw_dec_uri_handler_init)
)



static void gst_uade_raw_dec_finalize(GObject *object);

static void gst_uade_raw_dec_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec);
static void gst_uade_raw_dec_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec);

static gchar* gst_uade_raw_dec_get_filename(GstUadeBaseDec *dec, GError **error);



void gst_uade_raw_dec_class_init(GstUadeRawDecClass *klass)
{
	GObjectClass *object_class;
	GstElementClass *element_class;
	GstNonstreamAudioDecoderClass *nonstream_dec_class;
	GstUadeBaseDecClass *uade_base_dec_class;

	GST_DEBUG_CATEGORY_INIT(uaderawdec_debug, "uaderawdec", 0, "video game music player, raw version");

	object_class = G_OBJECT_CLASS(klass);
	element_class = GST_ELEMENT_CLASS(klass);
	nonstream_dec_class = GST_NONSTREAM_AUDIO_DECODER_CLASS(klass);
	uade_base_dec_class = GST_UADE_BASE_DEC_CLASS(klass);

	object_class->finalize = GST_DEBUG_FUNCPTR(gst_uade_raw_dec_finalize);
	object_class->set_property = GST_DEBUG_FUNCPTR(gst_uade_raw_dec_set_property);
	object_class->get_property = GST_DEBUG_FUNCPTR(gst_uade_raw_dec_get_property);

	// Many raw Amiga game tracks consist of multiple files. For example,
	// TFMX tracks consist of one MDAT and one SMPL file. For this reason,
	// we cannot load these from a sinkpad. We need an MDAT filename so we
	// can locate the corresponding SMPL (by replacing "mdat" with "smpl"
	// in the filename). By setting loads_from_sinkpad to FALSE, we disable
	// the sinkpad, and enable the load_from_custom vfunc.
	nonstream_dec_class->loads_from_sinkpad = FALSE;

	uade_base_dec_class->get_filename = GST_DEBUG_FUNCPTR(gst_uade_raw_dec_get_filename);

	g_object_class_install_property(
		object_class,
		PROP_LOCATION,
		g_param_spec_string(
			"location",
			"File Location",
			"Location of the music file to play",
			DEFAULT_LOCATION,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		)
	);

	// We include "Source" in the element classes, because we have to
	// open the music files directly (see the loads_from_sinkpad
	// comment above for more details).
	gst_element_class_set_static_metadata(
		element_class,
		"UADE Amiga music player, raw version",
		"Codec/Decoder/Source/Audio",
		"Plays Commodore Amiga game and demo music from raw files (usually ripped directly from game data)",
		"Carlos Rafael Giani <crg7475@mailbox.org>"
	);
}


void gst_uade_raw_dec_init(GstUadeRawDec *uade_raw_dec)
{
	uade_raw_dec->location = g_strdup(DEFAULT_LOCATION);
}


static void gst_uade_raw_dec_uri_handler_init(gpointer iface, G_GNUC_UNUSED gpointer iface_data)
{
	GstURIHandlerInterface *uri_handler_iface = (GstURIHandlerInterface *)iface;

	uri_handler_iface->get_type = gst_uade_raw_dec_uri_get_type;
	uri_handler_iface->get_protocols = gst_uade_raw_dec_uri_get_protocols;
	uri_handler_iface->get_uri = gst_uade_raw_dec_uri_get_uri;
	uri_handler_iface->set_uri = gst_uade_raw_dec_uri_set_uri;
}


static GstURIType gst_uade_raw_dec_uri_get_type(G_GNUC_UNUSED GType type)
{
	return GST_URI_SRC;
}


static const gchar* const * gst_uade_raw_dec_uri_get_protocols(G_GNUC_UNUSED GType type)
{
	return uade_supported_protocols;
}


static gchar* gst_uade_raw_dec_uri_get_uri(GstURIHandler *handler)
{
	gchar *uri;
	GstUadeRawDec *uade_raw_dec = GST_UADE_RAW_DEC(handler);

	GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(uade_raw_dec);
	uri = g_strjoin("", uade_uri_scheme, uade_raw_dec->location, NULL);
	GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(uade_raw_dec);

	return uri;
}


static gboolean gst_uade_raw_dec_uri_set_uri(GstURIHandler *handler, const gchar *uri, GError **error)
{
	GstUadeRawDec *uade_raw_dec = GST_UADE_RAW_DEC(handler);

	if (G_UNLIKELY(!g_str_has_prefix(uri, uade_uri_scheme)))
	{
		// TODO: check if there's a better error domain for invalid URLs
		g_set_error(error, G_FILE_ERROR, G_FILE_ERROR_INVAL, "Invalid URI %s", uri);
		return FALSE;
	}

	GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(uade_raw_dec);
	g_free(uade_raw_dec->location);
	uade_raw_dec->location = g_strdup(uri + (sizeof(uade_uri_scheme) - 1));
	GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(uade_raw_dec);

	return TRUE;
}


static void gst_uade_raw_dec_finalize(GObject *object)
{
	GstUadeRawDec *uade_raw_dec = GST_UADE_RAW_DEC(object);

	g_free(uade_raw_dec->location);

	G_OBJECT_CLASS(gst_uade_raw_dec_parent_class)->finalize(object);
}


static void gst_uade_raw_dec_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
	GstUadeRawDec *uade_raw_dec = GST_UADE_RAW_DEC(object);

	switch (prop_id)
	{
		case PROP_LOCATION:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			if (uade_raw_dec->location != NULL)
			{
				GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
				GST_ERROR_OBJECT(uade_raw_dec, "a music file is already opened; reopening is not supported");
				return;
			}
			g_free(uade_raw_dec->location);
			uade_raw_dec->location = g_strdup(g_value_get_string(value));
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}


static void gst_uade_raw_dec_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
	GstUadeRawDec *uade_raw_dec = GST_UADE_RAW_DEC(object);

	switch (prop_id)
	{
		case PROP_LOCATION:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			g_value_set_string(value, uade_raw_dec->location);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}


static gchar* gst_uade_raw_dec_get_filename(GstUadeBaseDec *uade_base_dec, G_GNUC_UNUSED GError **error)
{
	GstUadeRawDec *uade_raw_dec = GST_UADE_RAW_DEC(uade_base_dec);

	if (uade_raw_dec->location == NULL)
	{
		g_set_error_literal(error, G_FILE_ERROR, G_FILE_ERROR_INVAL, "no filename set");
		return NULL;
	}

	return g_strdup(uade_raw_dec->location);
}
