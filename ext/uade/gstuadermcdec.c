/*
 *   GStreamer plugin for decoding Amiga music with the Unix Amiga Delitracker Emulator (UADE)
 *   Copyright (C) 2018 Carlos Rafael Giani
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


#ifdef HAVE_CONFIG_H
 #include <config.h>
#endif

#include "gstuade.h"
#include "gstuadermcdec.h"
#include "gstuadebasedec.h"


GST_DEBUG_CATEGORY_STATIC(uadermcdec_debug);
#define GST_CAT_DEFAULT uadermcdec_debug




static GstStaticPadTemplate sink_template = GST_STATIC_PAD_TEMPLATE(
	"sink",
	GST_PAD_SINK,
	GST_PAD_ALWAYS,
	GST_STATIC_CAPS(
		GST_UADE_RMC_MEDIA_TYPE
	)
);




struct _GstUadeRmcDec
{
	GstUadeBaseDec parent;
};


struct _GstUadeRmcDecClass
{
	GstUadeBaseDecClass parent_class;
};



G_DEFINE_TYPE(GstUadeRmcDec, gst_uade_rmc_dec, GST_TYPE_UADE_BASE_DEC)



void gst_uade_rmc_dec_class_init(GstUadeRmcDecClass *klass)
{
	GstElementClass *element_class;
	GstNonstreamAudioDecoderClass *nonstream_dec_class;

	GST_DEBUG_CATEGORY_INIT(uadermcdec_debug, "uadermcdec", 0, "video game music player, RMC version");

	element_class = GST_ELEMENT_CLASS(klass);
	nonstream_dec_class = GST_NONSTREAM_AUDIO_DECODER_CLASS(klass);

	gst_element_class_add_pad_template(element_class, gst_static_pad_template_get(&sink_template));

	// RMC files contain everything needed to play the music, so we can
	// decode RMC data from any source, and we can load it from a sinkpad.
	// loads_from_sinkpad = TRUE enables the sinkpad.
	nonstream_dec_class->loads_from_sinkpad = TRUE;

	gst_element_class_set_static_metadata(
		element_class,
		"UADE Amiga music player, RMC version",
		"Codec/Decoder/Audio",
		"Plays Commodore Amiga game and demo music from RMC files",
		"Carlos Rafael Giani <crg7475@mailbox.org>"
	);
}


void gst_uade_rmc_dec_init(G_GNUC_UNUSED GstUadeRmcDec *uade_rmc_dec)
{
}
