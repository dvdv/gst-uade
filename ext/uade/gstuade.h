/*
 *   GStreamer plugin for decoding Amiga music with the Unix Amiga Delitracker Emulator (UADE)
 *   Copyright (C) 2018 Carlos Rafael Giani
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


#ifndef GSTUADE_H
#define GSTUADE_H

#include <gst/gst.h>


G_BEGIN_DECLS


typedef enum
{
	GST_UADE_FILTER_TYPE_A500,
	GST_UADE_FILTER_TYPE_A1200
} gst_uade_filter_types;


typedef enum
{
	GST_UADE_HEADPHONE_MODE_NONE,
	GST_UADE_HEADPHONE_MODE_1,
	GST_UADE_HEADPHONE_MODE_2
} gst_uade_headphone_modes;


typedef enum
{
	GST_UADE_VERTICAL_TIMING_MODE_PAL,
	GST_UADE_VERTICAL_TIMING_MODE_NTSC
} gst_uade_vertical_timing_mode;


#define GST_UADE_RMC_MEDIA_TYPE "audio/x-uade-rmc"


GType gst_uade_filter_type_get_type(void);
#define GST_TYPE_UADE_FILTER_TYPE (gst_uade_filter_type_get_type())

GType gst_uade_headphone_mode_get_type(void);
#define GST_TYPE_UADE_HEADPHONE_MODE (gst_uade_headphone_mode_get_type())

GType gst_uade_vertical_timing_mode_get_type(void);
#define GST_TYPE_UADE_VERTICAL_TIMING_MODE (gst_uade_vertical_timing_mode_get_type())


G_END_DECLS


#endif
