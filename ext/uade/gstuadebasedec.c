/*
 *   GStreamer plugin for decoding Amiga music with the Unix Amiga Delitracker Emulator (UADE)
 *   Copyright (C) 2018 Carlos Rafael Giani
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


#ifdef HAVE_CONFIG_H
 #include <config.h>
#endif

#include <gst/gst.h>
#include <uade/uade.h>

#include "gstuade.h"
#include "gstuadebasedec.h"


GST_DEBUG_CATEGORY_STATIC(uadebasedec_debug);
#define GST_CAT_DEFAULT uadebasedec_debug


enum
{
	PROP_0,
	PROP_UADECORE_FILE,
	PROP_BASE_DIRECTORY,
	PROP_SUBSONG_TIMEOUT,
	PROP_SILENCE_TIMEOUT,
	PROP_TOTAL_TIMEOUT,
	PROP_VERTICAL_TIMING_MODE,
	PROP_FILTER_TYPE,
	PROP_HEADPHONE_MODE,
	PROP_USE_FILTER,
	PROP_DETECT_SONG_END,
	PROP_GAIN,
	PROP_USE_POSTPROCESSING,
	PROP_PANNING,
	PROP_OUTPUT_BUFFER_SIZE
};


#define DEFAULT_UADECORE_FILE UADE_CONFIG_UADE_CORE
#define DEFAULT_BASE_DIRECTORY UADE_CONFIG_BASE_DIR
#define DEFAULT_SUBSONG_TIMEOUT 500
#define DEFAULT_SILENCE_TIMEOUT 20
#define DEFAULT_TOTAL_TIMEOUT -1
#define DEFAULT_VERTICAL_TIMING_MODE GST_UADE_VERTICAL_TIMING_MODE_PAL
#define DEFAULT_FILTER_TYPE GST_UADE_FILTER_TYPE_A500
#define DEFAULT_HEADPHONE_MODE GST_UADE_HEADPHONE_MODE_NONE
#define DEFAULT_USE_FILTER FALSE
#define DEFAULT_DETECT_SONG_END TRUE
#define DEFAULT_GAIN 1.0
#define DEFAULT_USE_POSTPROCESSING TRUE
#define DEFAULT_PANNING 0.0
#define DEFAULT_OUTPUT_BUFFER_SIZE 1024


// 1 frame = 2 16-bitsamples (because there are 2 channels)
#define NUM_BYTES_PER_FRAME (2 * 16 / 8)



static GstStaticPadTemplate src_template = GST_STATIC_PAD_TEMPLATE(
	"src",
	GST_PAD_SRC,
	GST_PAD_ALWAYS,
	GST_STATIC_CAPS(
		"audio/x-raw, "
		"format = (string) " GST_AUDIO_NE(S16) ", "
		"layout = (string) interleaved, "
		"rate = (int) [ 1, 48000 ], "
		"channels = (int) 2 "
	)
);




struct _GstUadeBaseDecPrivate
{
	struct uade_state *state;
	struct uade_song_info const *info;

	gchar *uadecore_file;
	gchar *base_directory;
	gint subsong_timeout;
	gint silence_timeout;
	gint total_timeout;
	gst_uade_vertical_timing_mode vertical_timing_mode;
	gst_uade_filter_types filter_type;
	gst_uade_headphone_modes headphone_mode;
	gboolean use_filter;
	gboolean detect_song_end;
	gdouble gain;
	gboolean use_postprocessing;
	gdouble panning;
	guint output_buffer_size;

	gboolean playback_started;
	guint current_subsong;

	GstBuffer *source_data;
};


typedef gboolean (*GstUadeBaseDecLoadFunction)(GstUadeBaseDec *uade_base_dec);



G_DEFINE_TYPE_WITH_CODE(
	GstUadeBaseDec, gst_uade_base_dec, GST_TYPE_NONSTREAM_AUDIO_DECODER,
	G_ADD_PRIVATE(GstUadeBaseDec)
)



static void gst_uade_base_dec_finalize(GObject *object);

static void gst_uade_base_dec_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec);
static void gst_uade_base_dec_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec);

static gboolean gst_uade_base_dec_load_from_buffer(GstNonstreamAudioDecoder *dec, GstBuffer *source_data, guint initial_subsong, GstNonstreamAudioSubsongMode initial_subsong_mode, GstClockTime *initial_position, GstNonstreamAudioOutputMode *initial_output_mode, gint *initial_num_loops);
static gboolean gst_uade_base_dec_load_from_custom(GstNonstreamAudioDecoder *dec, guint initial_subsong, GstNonstreamAudioSubsongMode initial_subsong_mode, GstClockTime *initial_position, GstNonstreamAudioOutputMode *initial_output_mode, gint *initial_num_loops);

static gboolean gst_uade_base_dec_set_current_subsong(GstNonstreamAudioDecoder *dec, guint subsong, GstClockTime *initial_position);
static guint gst_uade_base_dec_get_current_subsong(GstNonstreamAudioDecoder *dec);

static guint gst_uade_base_dec_get_num_subsongs(GstNonstreamAudioDecoder *dec);

static guint gst_uade_base_dec_get_supported_output_modes(GstNonstreamAudioDecoder *dec);
static gboolean gst_uade_base_dec_decode(GstNonstreamAudioDecoder *dec, GstBuffer **buffer, guint *num_samples);

static gboolean gst_uade_base_dec_do_load(GstUadeBaseDec *uade_base_dec, guint initial_subsong, GstNonstreamAudioSubsongMode initial_subsong_mode, GstClockTime *initial_position, GstNonstreamAudioOutputMode *initial_output_mode, gint *initial_num_loops, GstUadeBaseDecLoadFunction load_func);
static gboolean gst_uade_base_dec_load_from_filename(GstUadeBaseDec *uade_base_dec);
static gboolean gst_uade_base_dec_load_from_source_data(GstUadeBaseDec *uade_base_dec);



void gst_uade_base_dec_class_init(GstUadeBaseDecClass *klass)
{
	GObjectClass *object_class;
	GstElementClass *element_class;
	GstNonstreamAudioDecoderClass *dec_class;

	GST_DEBUG_CATEGORY_INIT(uadebasedec_debug, "uadebasedec", 0, "video game music player base class");

	object_class = G_OBJECT_CLASS(klass);
	element_class = GST_ELEMENT_CLASS(klass);
	dec_class = GST_NONSTREAM_AUDIO_DECODER_CLASS(klass);

	gst_element_class_add_pad_template(element_class, gst_static_pad_template_get(&src_template));

	object_class->finalize = GST_DEBUG_FUNCPTR(gst_uade_base_dec_finalize);
	object_class->set_property = GST_DEBUG_FUNCPTR(gst_uade_base_dec_set_property);
	object_class->get_property = GST_DEBUG_FUNCPTR(gst_uade_base_dec_get_property);

	dec_class->load_from_buffer = GST_DEBUG_FUNCPTR(gst_uade_base_dec_load_from_buffer);
	dec_class->load_from_custom = GST_DEBUG_FUNCPTR(gst_uade_base_dec_load_from_custom);

	dec_class->get_supported_output_modes = GST_DEBUG_FUNCPTR(gst_uade_base_dec_get_supported_output_modes);
	dec_class->decode = GST_DEBUG_FUNCPTR(gst_uade_base_dec_decode);

	dec_class->set_current_subsong = GST_DEBUG_FUNCPTR(gst_uade_base_dec_set_current_subsong);
	dec_class->get_current_subsong = GST_DEBUG_FUNCPTR(gst_uade_base_dec_get_current_subsong);
	dec_class->get_num_subsongs = GST_DEBUG_FUNCPTR(gst_uade_base_dec_get_num_subsongs);

	g_object_class_install_property(
		object_class,
		PROP_BASE_DIRECTORY,
		g_param_spec_string(
			"base-directory",
			"Base directory",
			"Directory containing eagleplayer.conf , the score file, and the players subdirectory",
			DEFAULT_BASE_DIRECTORY,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		)
	);
	g_object_class_install_property(
		object_class,
		PROP_SUBSONG_TIMEOUT,
		g_param_spec_int(
			"subsong-timeout",
			"Subsong timeout",
			"The number of seconds after which a subsong is forced to end (-1 = no timeout)",
			-1, G_MAXINT,
			DEFAULT_SUBSONG_TIMEOUT,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		)
	);
	g_object_class_install_property(
		object_class,
		PROP_SILENCE_TIMEOUT,
		g_param_spec_int(
			"silence-timeout",
			"Silence timeout",
			"The number of seconds of silence after which a subsong is ended automatically (-1 = no timeout)",
			-1, G_MAXINT,
			DEFAULT_SILENCE_TIMEOUT,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		)
	);
	g_object_class_install_property(
		object_class,
		PROP_TOTAL_TIMEOUT,
		g_param_spec_int(
			"total-timeout",
			"Total timeout",
			"The total number of seconds for all totals after which the song is forced to end (-1 = no timeout)",
			-1, G_MAXINT,
			DEFAULT_TOTAL_TIMEOUT,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		)
	);
	g_object_class_install_property(
		object_class,
		PROP_VERTICAL_TIMING_MODE,
		g_param_spec_enum(
			"vertical-timing-mode",
			"Vertical timing mode",
			"The timing for vertical blank interrupt timed songs (has no effect for replays using CIA / frequency indepent timing)",
			GST_TYPE_UADE_VERTICAL_TIMING_MODE,
			DEFAULT_VERTICAL_TIMING_MODE,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		)
	);
	g_object_class_install_property(
		object_class,
		PROP_FILTER_TYPE,
		g_param_spec_enum(
			"filter-type",
			"Playback filter type",
			"Lowpass filter for the audio playback",
			GST_TYPE_UADE_FILTER_TYPE,
			DEFAULT_FILTER_TYPE,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		)
	);
	g_object_class_install_property(
		object_class,
		PROP_HEADPHONE_MODE,
		g_param_spec_enum(
			"headphone-mode",
			"Headphone mode",
			"Headphone output mode to use",
			GST_TYPE_UADE_HEADPHONE_MODE,
			DEFAULT_HEADPHONE_MODE,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		)
	);
	g_object_class_install_property(
		object_class,
		PROP_USE_FILTER,
		g_param_spec_boolean(
			"use-filter",
			"Use the lowpass filter",
			"Whether or not to use the configured lowpass filter",
			DEFAULT_USE_FILTER,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		)
	);
	g_object_class_install_property(
		object_class,
		PROP_DETECT_SONG_END,
		g_param_spec_boolean(
			"detect-song-end",
			"Detect song end",
			"Enable song end detection to make the player terminate when there is nothing more to play (disabling this keeps playing, which might loop the music or just produce silence); this does not affect timeouts",
			DEFAULT_DETECT_SONG_END,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		)
	);
	g_object_class_install_property(
		object_class,
		PROP_GAIN,
		g_param_spec_double(
			"gain",
			"Gain",
			"Gain to apply on the output; 0.0 = silence  1.0 = 100% (no change)",
			0.0, 128.0,
			DEFAULT_GAIN,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		)
	);
	g_object_class_install_property(
		object_class,
		PROP_USE_POSTPROCESSING,
		g_param_spec_boolean(
			"use-postprocessing",
			"Use postprocessing",
			"Whether or not to use postprocessing effects (if set to FALSE, this disables: headphone mode, panning, gain)",
			DEFAULT_USE_POSTPROCESSING,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		)
	);
	g_object_class_install_property(
		object_class,
		PROP_PANNING,
		g_param_spec_double(
			"panning",
			"Panning amount",
			"Amount of panning to apply; 0.0 = full stereo  1.0 = mono  2.0 = inverse stereo",
			0.0, 2.0,
			DEFAULT_PANNING,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		)
	);
	g_object_class_install_property(
		object_class,
		PROP_OUTPUT_BUFFER_SIZE,
		g_param_spec_uint(
			"output-buffer-size",
			"Output buffer size",
			"Size of each output buffer, in samples (actual size can be smaller than this during flush or EOS)",
			1, G_MAXUINT,
			DEFAULT_OUTPUT_BUFFER_SIZE,
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS
		)
	);
}


void gst_uade_base_dec_init(GstUadeBaseDec *uade_base_dec)
{
	uade_base_dec->priv = gst_uade_base_dec_get_instance_private(uade_base_dec);

	uade_base_dec->priv->state = NULL;
	uade_base_dec->priv->info  = NULL;

	uade_base_dec->priv->uadecore_file        = g_strdup(DEFAULT_UADECORE_FILE);
	uade_base_dec->priv->base_directory       = g_strdup(DEFAULT_BASE_DIRECTORY);
	uade_base_dec->priv->subsong_timeout      = DEFAULT_SUBSONG_TIMEOUT;
	uade_base_dec->priv->silence_timeout      = DEFAULT_SILENCE_TIMEOUT;
	uade_base_dec->priv->total_timeout        = DEFAULT_TOTAL_TIMEOUT;
	uade_base_dec->priv->vertical_timing_mode = DEFAULT_VERTICAL_TIMING_MODE;
	uade_base_dec->priv->filter_type          = DEFAULT_FILTER_TYPE;
	uade_base_dec->priv->detect_song_end      = DEFAULT_DETECT_SONG_END;
	uade_base_dec->priv->headphone_mode       = DEFAULT_HEADPHONE_MODE;
	uade_base_dec->priv->use_filter           = DEFAULT_USE_FILTER;
	uade_base_dec->priv->gain                 = DEFAULT_GAIN;
	uade_base_dec->priv->use_postprocessing   = DEFAULT_USE_POSTPROCESSING;
	uade_base_dec->priv->panning              = DEFAULT_PANNING;
	uade_base_dec->priv->output_buffer_size   = DEFAULT_OUTPUT_BUFFER_SIZE;

	uade_base_dec->priv->playback_started = FALSE;
	uade_base_dec->priv->current_subsong  = 0;
}


static void gst_uade_base_dec_finalize(GObject *object)
{
	GstUadeBaseDec *uade_base_dec;

	g_return_if_fail(GST_IS_UADE_BASE_DEC(object));
	uade_base_dec = GST_UADE_BASE_DEC(object);

	if (uade_base_dec->priv->playback_started)
		uade_stop(uade_base_dec->priv->state);
	uade_cleanup_state(uade_base_dec->priv->state);

	g_free(uade_base_dec->priv->uadecore_file);
	g_free(uade_base_dec->priv->base_directory);

	G_OBJECT_CLASS(gst_uade_base_dec_parent_class)->finalize(object);
}


static void gst_uade_base_dec_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
	GstUadeBaseDec *uade_base_dec = GST_UADE_BASE_DEC(object);

#define CHECK_IF_ALREADY_INITIALIZED() \
	do { \
		if (uade_base_dec->priv->state != NULL) \
		{ \
			GST_ERROR_OBJECT(uade_base_dec, "changes to the %s property after playback already started are not supported", g_param_spec_get_name(pspec)); \
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object); \
			return; \
		} \
	} while (0)

#define UPDATE_EXISTING_NUM_CONFIG(OPTION, VALUE) \
	do { \
		if (uade_base_dec->priv->state != NULL) \
		{ \
			gchar *tmpstr = g_strdup_printf("%f", (VALUE)); \
			struct uade_config *cfg = uade_get_effective_config(uade_base_dec->priv->state); \
			uade_config_set_option(cfg, (OPTION), tmpstr); \
			g_free(tmpstr); \
		} \
	} while (0)

	switch (prop_id)
	{
		case PROP_UADECORE_FILE:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			CHECK_IF_ALREADY_INITIALIZED();
			g_free(uade_base_dec->priv->uadecore_file);
			uade_base_dec->priv->uadecore_file = g_strdup(g_value_get_string(value));
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_BASE_DIRECTORY:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			CHECK_IF_ALREADY_INITIALIZED();
			g_free(uade_base_dec->priv->base_directory);
			uade_base_dec->priv->base_directory = g_strdup(g_value_get_string(value));
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_SUBSONG_TIMEOUT:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			CHECK_IF_ALREADY_INITIALIZED();
			uade_base_dec->priv->subsong_timeout = g_value_get_int(value);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_SILENCE_TIMEOUT:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			CHECK_IF_ALREADY_INITIALIZED();
			uade_base_dec->priv->silence_timeout = g_value_get_int(value);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_TOTAL_TIMEOUT:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			CHECK_IF_ALREADY_INITIALIZED();
			uade_base_dec->priv->total_timeout = g_value_get_int(value);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_VERTICAL_TIMING_MODE:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			CHECK_IF_ALREADY_INITIALIZED();
			uade_base_dec->priv->vertical_timing_mode = g_value_get_enum(value);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_FILTER_TYPE:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			CHECK_IF_ALREADY_INITIALIZED();
			uade_base_dec->priv->filter_type = g_value_get_enum(value);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_HEADPHONE_MODE:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			CHECK_IF_ALREADY_INITIALIZED();
			uade_base_dec->priv->headphone_mode = g_value_get_enum(value);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_USE_FILTER:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			uade_base_dec->priv->use_filter = g_value_get_boolean(value);
			if (uade_base_dec->priv->state != NULL)
				uade_set_filter_state(uade_base_dec->priv->state, uade_base_dec->priv->use_filter);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_DETECT_SONG_END:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			CHECK_IF_ALREADY_INITIALIZED();
			uade_base_dec->priv->detect_song_end = g_value_get_boolean(value);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_GAIN:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			uade_base_dec->priv->gain = g_value_get_double(value);
			UPDATE_EXISTING_NUM_CONFIG(UC_GAIN, uade_base_dec->priv->gain);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_USE_POSTPROCESSING:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			uade_base_dec->priv->use_postprocessing = g_value_get_boolean(value);
			if (uade_base_dec->priv->state != NULL)
				uade_effect_enable(uade_base_dec->priv->state, UADE_EFFECT_ALLOW);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_PANNING:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			uade_base_dec->priv->panning = g_value_get_double(value);
			UPDATE_EXISTING_NUM_CONFIG(UC_PANNING_VALUE, uade_base_dec->priv->panning);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_OUTPUT_BUFFER_SIZE:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			CHECK_IF_ALREADY_INITIALIZED();
			uade_base_dec->priv->output_buffer_size = g_value_get_uint(value);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}

#undef CHECK_IF_ALREADY_INITIALIZED
#undef UPDATE_EXISTING_NUM_CONFIG
}


static void gst_uade_base_dec_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
	GstUadeBaseDec *uade_base_dec = GST_UADE_BASE_DEC(object);

	switch (prop_id)
	{
		case PROP_UADECORE_FILE:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			g_value_set_string(value, uade_base_dec->priv->uadecore_file);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_BASE_DIRECTORY:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			g_value_set_string(value, uade_base_dec->priv->base_directory);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_SUBSONG_TIMEOUT:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			g_value_set_int(value, uade_base_dec->priv->subsong_timeout);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_SILENCE_TIMEOUT:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			g_value_set_int(value, uade_base_dec->priv->silence_timeout);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_TOTAL_TIMEOUT:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			g_value_set_int(value, uade_base_dec->priv->total_timeout);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_VERTICAL_TIMING_MODE:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			g_value_set_enum(value, uade_base_dec->priv->vertical_timing_mode);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_FILTER_TYPE:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			g_value_set_enum(value, uade_base_dec->priv->filter_type);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_HEADPHONE_MODE:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			g_value_set_enum(value, uade_base_dec->priv->headphone_mode);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_USE_FILTER:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			g_value_set_boolean(value, uade_base_dec->priv->use_filter);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_DETECT_SONG_END:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			g_value_set_boolean(value, uade_base_dec->priv->detect_song_end);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_GAIN:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			g_value_set_double(value, uade_base_dec->priv->gain);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_USE_POSTPROCESSING:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			g_value_set_boolean(value, uade_base_dec->priv->use_postprocessing);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_PANNING:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			g_value_set_double(value, uade_base_dec->priv->panning);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		case PROP_OUTPUT_BUFFER_SIZE:
			GST_NONSTREAM_AUDIO_DECODER_LOCK_MUTEX(object);
			g_value_set_uint(value, uade_base_dec->priv->output_buffer_size);
			GST_NONSTREAM_AUDIO_DECODER_UNLOCK_MUTEX(object);
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}


static gboolean gst_uade_base_dec_load_from_buffer(GstNonstreamAudioDecoder *dec, GstBuffer *source_data, guint initial_subsong, GstNonstreamAudioSubsongMode initial_subsong_mode, GstClockTime *initial_position, GstNonstreamAudioOutputMode *initial_output_mode, gint *initial_num_loops)
{
	GstUadeBaseDec *uade_base_dec = GST_UADE_BASE_DEC(dec);
	uade_base_dec->priv->source_data = source_data;
	return gst_uade_base_dec_do_load(
		uade_base_dec,
		initial_subsong,
		initial_subsong_mode,
		initial_position,
		initial_output_mode,
		initial_num_loops,
		gst_uade_base_dec_load_from_source_data
	);
}


static gboolean gst_uade_base_dec_load_from_custom(GstNonstreamAudioDecoder *dec, guint initial_subsong, GstNonstreamAudioSubsongMode initial_subsong_mode, GstClockTime *initial_position, GstNonstreamAudioOutputMode *initial_output_mode, gint *initial_num_loops)
{
	GstUadeBaseDec *uade_base_dec = GST_UADE_BASE_DEC(dec);
	return gst_uade_base_dec_do_load(
		uade_base_dec,
		initial_subsong,
		initial_subsong_mode,
		initial_position,
		initial_output_mode,
		initial_num_loops,
		gst_uade_base_dec_load_from_filename
	);
}


static gboolean gst_uade_base_dec_set_current_subsong(GstNonstreamAudioDecoder *dec, guint subsong, GstClockTime *initial_position)
{
	GstUadeBaseDec *uade_base_dec = GST_UADE_BASE_DEC(dec);

	g_return_val_if_fail(uade_base_dec->priv->state != NULL, FALSE);

	// Limit current subsong index to the valid range
	uade_base_dec->priv->current_subsong = CLAMP(((int)subsong) + uade_base_dec->priv->info->subsongs.min, uade_base_dec->priv->info->subsongs.min, uade_base_dec->priv->info->subsongs.max);

	// Subsong always starts at the beginning
	*initial_position = 0;

	return (uade_seek(UADE_SEEK_SUBSONG_RELATIVE, 0, uade_base_dec->priv->current_subsong, uade_base_dec->priv->state) == 0);
}


static guint gst_uade_base_dec_get_current_subsong(GstNonstreamAudioDecoder *dec)
{
	GstUadeBaseDec *uade_base_dec = GST_UADE_BASE_DEC(dec);
	return (uade_base_dec->priv->info == NULL) ? 0 : (uade_base_dec->priv->current_subsong - uade_base_dec->priv->info->subsongs.min);
}


static guint gst_uade_base_dec_get_num_subsongs(GstNonstreamAudioDecoder *dec)
{
	GstUadeBaseDec *uade_base_dec = GST_UADE_BASE_DEC(dec);
	return (uade_base_dec->priv->info == NULL) ? 0 : (uade_base_dec->priv->info->subsongs.max - uade_base_dec->priv->info->subsongs.min + 1);
}


static guint gst_uade_base_dec_get_supported_output_modes(G_GNUC_UNUSED GstNonstreamAudioDecoder *dec)
{
	return 1u << GST_NONSTREAM_AUDIO_OUTPUT_MODE_STEADY;
}


static gboolean gst_uade_base_dec_decode(GstNonstreamAudioDecoder *dec, GstBuffer **buffer, guint *num_samples)
{
	GstBuffer *outbuf;
	GstMapInfo map;
	GstUadeBaseDec *uade_base_dec;
	long num_bytes_per_outbuf, actual_num_frames_read, actual_num_bytes_read;

	uade_base_dec = GST_UADE_BASE_DEC(dec);

	num_bytes_per_outbuf = uade_base_dec->priv->output_buffer_size * NUM_BYTES_PER_FRAME;

	outbuf = gst_nonstream_audio_decoder_allocate_output_buffer(dec, num_bytes_per_outbuf);
	if (G_UNLIKELY(outbuf == NULL))
		return FALSE;

	gst_buffer_map(outbuf, &map, GST_MAP_WRITE);
	actual_num_bytes_read = uade_read(map.data, map.size, uade_base_dec->priv->state);
	gst_buffer_unmap(outbuf, &map);

	actual_num_frames_read = actual_num_bytes_read / NUM_BYTES_PER_FRAME;

	GST_TRACE_OBJECT(dec, "read %ld byte", actual_num_bytes_read);

	if (actual_num_frames_read > 0)
	{
		if (actual_num_bytes_read != num_bytes_per_outbuf)
			gst_buffer_set_size(outbuf, actual_num_bytes_read);

		*buffer = outbuf;
		*num_samples = actual_num_frames_read;
	}
	else
	{
		if (actual_num_bytes_read == 0)
			GST_INFO_OBJECT(uade_base_dec, "UADE reached end of song");
		else if (actual_num_bytes_read < 0)
			GST_ERROR_OBJECT(uade_base_dec, "UADE reported error during playback - shutting down");
		else
			GST_WARNING_OBJECT(uade_base_dec, "only %ld byte decoded", actual_num_bytes_read);

		gst_buffer_unref(outbuf);
		*buffer = NULL;
		*num_samples = 0;
	}

	return ((*buffer) != NULL);
}


static gboolean gst_uade_base_dec_do_load(GstUadeBaseDec *uade_base_dec, guint initial_subsong, G_GNUC_UNUSED GstNonstreamAudioSubsongMode initial_subsong_mode, GstClockTime *initial_position, GstNonstreamAudioOutputMode *initial_output_mode, G_GNUC_UNUSED gint *initial_num_loops, GstUadeBaseDecLoadFunction load_func)
{
	gboolean loaded;
	GstTagList *tags;
	struct uade_config *config;
	gchar *tmpstr;

	g_assert(load_func != NULL);

	g_assert(uade_base_dec->priv->state == NULL);

	GST_DEBUG_OBJECT(uade_base_dec, "attempting to load music file");


	/* Assemble a new UADE configuration with the options set from our GObject properties. */

	config = uade_new_config();

	uade_config_set_option(config, UC_ONE_SUBSONG, NULL);
	if (!uade_base_dec->priv->detect_song_end)
		uade_config_set_option(config, UC_NO_EP_END, NULL);
	uade_config_set_option(config, UC_RESAMPLER, "default");

	uade_config_set_option(config, UC_UADECORE_FILE, uade_base_dec->priv->uadecore_file);
	uade_config_set_option(config, UC_BASE_DIR, uade_base_dec->priv->base_directory);

	switch (uade_base_dec->priv->vertical_timing_mode)
	{
		case GST_UADE_VERTICAL_TIMING_MODE_PAL:  uade_config_set_option(config, UC_PAL, NULL); break;
		case GST_UADE_VERTICAL_TIMING_MODE_NTSC: uade_config_set_option(config, UC_NTSC, NULL); break;
		default: break;
	}
	switch (uade_base_dec->priv->filter_type)
	{
		case GST_UADE_FILTER_TYPE_A500:  uade_config_set_option(config, UC_FILTER_TYPE, "a500"); break;
		case GST_UADE_FILTER_TYPE_A1200: uade_config_set_option(config, UC_FILTER_TYPE, "a1200"); break;
		default: break;
	}
	switch (uade_base_dec->priv->headphone_mode)
	{
		case GST_UADE_HEADPHONE_MODE_NONE: uade_config_set_option(config, UC_NO_HEADPHONES, NULL); break;
		case GST_UADE_HEADPHONE_MODE_1:    uade_config_set_option(config, UC_HEADPHONES, NULL); break;
		case GST_UADE_HEADPHONE_MODE_2:    uade_config_set_option(config, UC_HEADPHONES2, NULL); break;
		default: break;
	}

	tmpstr = g_strdup_printf("%d", uade_base_dec->priv->subsong_timeout);
	uade_config_set_option(config, UC_SUBSONG_TIMEOUT_VALUE, tmpstr);
	g_free(tmpstr);

	tmpstr = g_strdup_printf("%d", uade_base_dec->priv->silence_timeout);
	uade_config_set_option(config, UC_SILENCE_TIMEOUT_VALUE, tmpstr);
	g_free(tmpstr);

	tmpstr = g_strdup_printf("%d", uade_base_dec->priv->total_timeout);
	uade_config_set_option(config, UC_TIMEOUT_VALUE, tmpstr);
	g_free(tmpstr);

	if (!(uade_base_dec->priv->use_filter))
		uade_config_set_option(config, UC_NO_FILTER, NULL); /* this must be called AFTER the filter type is set */

	tmpstr = g_strdup_printf("%f", uade_base_dec->priv->gain);
	uade_config_set_option(config, UC_GAIN, tmpstr);
	g_free(tmpstr);

	if (!(uade_base_dec->priv->use_postprocessing))
		uade_config_set_option(config, UC_NO_POSTPROCESSING, NULL);

	tmpstr = g_strdup_printf("%f", uade_base_dec->priv->panning);
	uade_config_set_option(config, UC_PANNING_VALUE, tmpstr);
	g_free(tmpstr);


	/* Create new UADE state object with the configuration. */
	uade_base_dec->priv->state = uade_new_state(config);

	/* We no longer need the configuration anymore, so we can discard it. */
	free(config);


	/* Set output format */
	gst_nonstream_audio_decoder_set_output_format_simple(
		GST_NONSTREAM_AUDIO_DECODER(uade_base_dec),
		uade_get_sampling_rate(uade_base_dec->priv->state),
		GST_AUDIO_FORMAT_S16,
		2
	);

	/* Now load the music with the supplied load function. */
	loaded = load_func(uade_base_dec);
	if (!loaded)
	{
		GST_ERROR_OBJECT(uade_base_dec, "loading song failed");
		return FALSE;
	}
	GST_TRACE_OBJECT(uade_base_dec, "loading successful, retrieving song information");
	uade_base_dec->priv->playback_started = TRUE;


	/* Retrieve song information from the UADE state block so we can provide
	 * some metadata to GStreamer and get information about the subsongs. */

	uade_base_dec->priv->info = uade_get_song_info(uade_base_dec->priv->state);
	if (uade_base_dec->priv->info == NULL)
	{
		GST_ERROR_OBJECT(uade_base_dec, "uade_get_song_info failed");
		return FALSE;
	}

	GST_INFO_OBJECT(uade_base_dec, "min subsong: %d  max subsong: %d", uade_base_dec->priv->info->subsongs.min, uade_base_dec->priv->info->subsongs.max);

	uade_base_dec->priv->current_subsong = CLAMP(((int)initial_subsong) + uade_base_dec->priv->info->subsongs.min, uade_base_dec->priv->info->subsongs.min, uade_base_dec->priv->info->subsongs.max);
	if (uade_seek(UADE_SEEK_SUBSONG_RELATIVE, 0, uade_base_dec->priv->current_subsong, uade_base_dec->priv->state) != 0)
	{
		GST_ERROR_OBJECT(uade_base_dec, "seeking to initial subsong failed");
		return FALSE;
	}

	// Playback always starts at the beginning, and is always steady,
	// because UADE does its own internal looping (based on the format).
	*initial_position = 0;
	*initial_output_mode = GST_NONSTREAM_AUDIO_OUTPUT_MODE_STEADY;

	// Pass the UADE song metadata to a GstTagList and push it out.
	tags = gst_tag_list_new_empty();
	if (uade_base_dec->priv->info->modulename[0] != 0)
		gst_tag_list_add(tags, GST_TAG_MERGE_APPEND, GST_TAG_TITLE, uade_base_dec->priv->info->modulename, NULL);
	if (uade_base_dec->priv->info->formatname[0] != 0)
		gst_tag_list_add(tags, GST_TAG_MERGE_APPEND, GST_TAG_CONTAINER_FORMAT, uade_base_dec->priv->info->formatname, NULL);
	if (uade_base_dec->priv->info->playername[0] != 0)
		gst_tag_list_add(tags, GST_TAG_MERGE_APPEND, GST_TAG_APPLICATION_NAME, uade_base_dec->priv->info->playername, NULL);
	gst_pad_push_event(GST_NONSTREAM_AUDIO_DECODER_SRC_PAD(uade_base_dec), gst_event_new_tag(tags));

	return TRUE;
}


static gboolean gst_uade_base_dec_load_from_filename(GstUadeBaseDec *uade_base_dec)
{
	int ret;
	gchar *filename;
	GstUadeBaseDecClass *klass;
	GError *error;

	klass = GST_UADE_BASE_DEC_CLASS(G_OBJECT_GET_CLASS(uade_base_dec));
	g_assert(klass->get_filename != NULL);

	error = NULL;
	filename = klass->get_filename(uade_base_dec, &error);
	if (filename == NULL)
	{
		GST_ELEMENT_ERROR(uade_base_dec, RESOURCE, OPEN_READ, ("could not get filename"), ("%s", error->message));
		g_error_free(error);
		return FALSE;
	}
	GST_TRACE_OBJECT(uade_base_dec, "loading music file from filename \"%s\"", filename);

	ret = uade_play(filename, -1, uade_base_dec->priv->state);

	g_free(filename);

	return !!ret;
}


static gboolean gst_uade_base_dec_load_from_source_data(GstUadeBaseDec *uade_base_dec)
{
	int ret;
	GstMapInfo map_info;

	g_assert(uade_base_dec->priv->source_data != NULL);

	gst_buffer_map(uade_base_dec->priv->source_data, &map_info, GST_MAP_READ);
	ret = uade_play_from_buffer(NULL, map_info.data, map_info.size, -1, uade_base_dec->priv->state);
	gst_buffer_unmap(uade_base_dec->priv->source_data, &map_info);

	return !!ret;
}
