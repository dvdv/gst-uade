/*
 *   GStreamer plugin for decoding Amiga music with the Unix Amiga Delitracker Emulator (UADE)
 *   Copyright (C) 2018 Carlos Rafael Giani
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


#ifndef GSTUADERAWDEC_H
#define GSTUADERAWDEC_H

#include <gst/gst.h>


G_BEGIN_DECLS


typedef struct _GstUadeRawDec GstUadeRawDec;
typedef struct _GstUadeRawDecClass GstUadeRawDecClass;


#define GST_TYPE_UADE_RAW_DEC             (gst_uade_raw_dec_get_type())
#define GST_UADE_RAW_DEC(obj)             (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_UADE_RAW_DEC, GstUadeRawDec))
#define GST_UADE_RAW_DEC_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_UADE_RAW_DEC, GstUadeRawDecClass))
#define GST_IS_UADE_RAW_DEC(obj)          (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_UADE_RAW_DEC))
#define GST_IS_UADE_RAW_DEC_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_UADE_RAW_DEC))


GType gst_uade_raw_dec_get_type(void);


G_END_DECLS


#endif
