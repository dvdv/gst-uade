/*
 *   GStreamer plugin for decoding Amiga music with the Unix Amiga Delitracker Emulator (UADE)
 *   Copyright (C) 2018 Carlos Rafael Giani
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


#ifndef GSTUADEBASEDEC_H
#define GSTUADEBASEDEC_H

#include <gst/gst.h>
#include "gst/audio/gstnonstreamaudiodecoder.h"


G_BEGIN_DECLS


typedef struct _GstUadeBaseDec GstUadeBaseDec;
typedef struct _GstUadeBaseDecClass GstUadeBaseDecClass;
typedef struct _GstUadeBaseDecPrivate GstUadeBaseDecPrivate;


#define GST_TYPE_UADE_BASE_DEC             (gst_uade_base_dec_get_type())
#define GST_UADE_BASE_DEC(obj)             (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_UADE_BASE_DEC, GstUadeBaseDec))
#define GST_UADE_BASE_DEC_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_UADE_BASE_DEC, GstUadeBaseDecClass))
#define GST_IS_UADE_BASE_DEC(obj)          (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_UADE_BASE_DEC))
#define GST_IS_UADE_BASE_DEC_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_UADE_BASE_DEC))


/**
 * GstUadeBaseDec:
 *
 * The opaque #GstUadeBaseDec data structure.
 */
struct _GstUadeBaseDec
{
	GstNonstreamAudioDecoder parent;

	/*< private > */
	GstUadeBaseDecPrivate *priv;
};


/**
 * GstUadeBaseDecClass:
 * @parent_class:    The parent class structure
 * @get_filename:    Returns a filename to be used for loading raw Amiga music
 *                   media. If an error occurs, this returns NULL, and the pointer
 *                   pointed to by GError is set to a new GError instance
 *                   containing an error report (unless the pointer is NULL).
 *
 * Implements the bulk of a decoder based on the Unix Amiga Delitracker Emulator
 * (UADE) software. This base class can be used for raw Amiga music decoders
 * and for UADE RMC file decoders. In the former case, @get_filename has to be
 * implemented, and GstNonstreamAudioDecoder @loads_from_sinkpad has to be set
 * to FALSE. If RMC files are to be decoded, @loads_from_sinkpad has to be set
 * to TRUE, and the @get_filename vfunc does not have to be implemented.
 */
struct _GstUadeBaseDecClass
{
	GstNonstreamAudioDecoderClass parent_class;

	gchar* (*get_filename)(GstUadeBaseDec * dec, GError **error);
};


GType gst_uade_base_dec_get_type(void);


G_END_DECLS


#endif
